<?php

class Braincloud
{
   var $items;		#hashtags items
   var $matrix;		#hashtags relations matrix
   var $relations;	#hashtags relations paths
   var $vid;		#vocabulary ID
   var $options;	#$view options
   
   public function Braincloud ($options)
   {
     $this->options = $options;
     $this->vid = $options['braincloud_vid'];
     $this->_getBraintags();
   }	
   
   private function _getBraintags()
   {
     $terms = taxonomy_get_tree($this->vid,0);
     foreach($terms as $key => $tag) {
       $this->items[$tag->tid] = new Braintag($tag); 
     }
     $this->_buildRelationMatrix();
   }
   
   private function _buildRelationMatrix()
   {
     $terms = taxonomy_get_tree($this->vid);
     foreach($terms as $vKey => $vTag) {
       foreach($terms as $hKey => $hTag) {
          $this->matrix[$vTag->tid][$hTag->tid] = 0;
       }
     }
     
     foreach($terms as $key => $tag) {
       foreach($tag->parents as $pKey => $pTid) {
         if ($pTid != 0) {
            $this->matrix[$tag->tid][$pTid] = 1;
            $this->relations[md5($tag->tid+$pTid)] = array($tag->tid,$pTid);
         }
       }
     }
   }
   
}