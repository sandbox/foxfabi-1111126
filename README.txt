$Id: README.txt 41 2011-03-30 15:28:14Z foxfabi $

ABOUT
------------------------------------------------------------------------
This module provides an additional Views plugins, named Braincloud.
Braincloud displays taxonomy terms in a HTML5 canvas element using Processing.js.
The objects are influenced by the vocabulary heirarchy. Every parent attracts his childs.

Processing.js is the sister project of the popular Processing visual programming language.

It's not magic, but almost.

Custom settings are supported.

For added fun, configure the object associated to display a term. //TODO

Views_Braincloud was developed by Fabian Dennler but never released. 
Feel free to move forward. Grow and succeed.

FAQ
------------------------------------------------------------------------
* Where to get more fonts?
  Check out processing.org file converter
  
* My Browser does not show anything?!
  Check for HTML5 compatibility and test with framerate=1
  Best result reached with Chrome...

* Fullscreen Background canvas?
  plug-in settings: screen.width, screen.height
  page.tpl
    <div class="clouds">
    #$view = views_get_view('hashtags');
    #print $view->execute_display('page_1');
	</div>

TODO / FIXME	
------------------------------------------------------------------------
 * fixme: plug-in options forms fields validation
 * term object display relation or code
 * movement code config
 * interacation with terms (filter?)
 
AUTHOR AND CREDIT
------------------------------------------------------------------------
 * Original Development # Fabian Dennler "foxfabi" (http://drupal.org/user/240264)