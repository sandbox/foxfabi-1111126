<?php
// $Id: views_braincloud.views.inc 41 2011-03-30 15:28:14Z foxfabi $

/**
 * @file
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_braincloud_views_plugins() {
  return array(
    'style' => array(
      'views_braincloud' => array(
        'title' => t('Braincloud'),
        'theme' => 'views_view_views_braincloud',
        'help' => t('views_braincloud help.'),
        'handler' => 'views_braincloud_style_plugin',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
