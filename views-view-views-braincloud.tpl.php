<?php
// $Id: views-view-views-braincloud.tpl.php 41 2011-03-30 15:28:14Z foxfabi $

/**
 * @file
 */

$module_dir = drupal_get_path('module', 'views_braincloud');
drupal_add_css($module_dir . "/css/braincloud.css");
drupal_add_js($module_dir . "/js/gettagsize.js");
drupal_add_js($module_dir . "/js/processing.init.js");
drupal_add_js($module_dir . "/js/processing-1.1.0.js");

#print_r($options);


$graph = new Braincloud($options);
if (sizeof($graph->items) == 0) {
  echo t('Error!! no Items found');
  return;
}

echo '<div style="background:'.$options['braincloud_bg'].'" id="clouds">';
?>
<div style="overflow:hidden;"><canvas style="border:0px solid black;" width="<?php echo $options['braincloud_width'].'px';?>" height="<?php echo $options['braincloud_height'].'px';?>"></canvas></div>
<div style="border:0px solid black; overflow:hidden;position:absolute:top:0px" id="srvresponse"></div>
</div>

<?php
/* END of your changes ;)
/********************************************************************************************/
?>
<script type="application/processing">
int numBalls = <?php echo sizeof($graph->items);  ?>;
Ball[] balls = new Ball[numBalls];  
String fontFile = "<?php echo $module_dir;?>/fonts/<?php echo $options['braincloud_font']?>.vlw";
int sampleRate = <?php echo $options['braincloud_framerate']?>;
color bg = <?php echo $options['braincloud_bg']?>;
int ajaxReload = <?php echo $options['braincloud_ajax']?>;
int minFontSize = <?php echo $options['braincloud_fontsize']?>; 
float spring = <?php echo $options['braincloud_spring']?>; 
int randomPos = <?php echo $options['braincloud_startpos']?>; 
int termAttraction = <?php echo $options['braincloud_attract']?>; 
int termBounce = <?php echo $options['braincloud_collide']?>; 

// Setup the Processing Canvas
void setup(){
  //hint(ENABLE_NATIVE_FONTS); // TODO:FIXME
  size(int(<?php echo $options['braincloud_width']?>),int(<?php echo $options['braincloud_height']?>), P2D);
  smooth();
  strokeWeight( 1 );
  frameRate( sampleRate );
  //cursor(CROSS);
  rectMode(CENTER);
  font = loadFont(fontFile);
  textFont(font);
  stroke(0,20);
  background(bg, 0.9);  
  termIndex = new ArrayList();  // store index:tid relation
  
  <?php
  // draw nodes
  $index = 0;
  foreach($graph->items as $tid => $hashTag) {
    echo "    balls[".$index."] = new Ball(".$index.", ".$hashTag->item->tid.", '".$hashTag->item->name."', ".$hashTag->item->size.", ".$hashTag->item->depth.");  "."\n";
    $index++;
  }
  $index = 0;
  foreach($graph->items as $tid => $hashTag) {
    if (is_array($graph->relations)) {
      foreach($graph->relations as $rid => $hashTagNodeArray) {
        // $hashTagNodeArray : source(parent) destination(child)
        if ($hashTagNodeArray[0] == $hashTag->item->tid) {
          echo "    balls[".$index."].addParent(".$hashTagNodeArray[1].");  "."\n";
        }
        if ($hashTagNodeArray[1] == $hashTag->item->tid) {
          echo "    balls[".$index."].addChild(".$hashTagNodeArray[0].");  "."\n";
        }
      }
    }
    echo "    balls[".$index."].calcAngle();  "."\n";
    $index++;
  }
  ?>
}


// Main draw loop
void draw(){
  background(bg, 0.9);  
  
  // reset show status (only once)
  for (int i = 0; i < numBalls; i++) { 
    balls[i].thisTurn = 0;
  }
  // display term related to parents
  for (int i = 0; i < numBalls; i++) { 
    if (balls[i].initDepth <= 0) { 
      balls[i].run();
    }
  }
  
  // check for collison
  for (int i = 0; i < numBalls; i++) { 
    if (termBounce) {
      balls[i].collide();
    }
  }
}

<?php include($module_dir . "/js/braincloud.js"); ?>
</script>
