function get_tag_size(id,result) {
  $.getTagSize = "";
  $.ajax({
    url: Drupal.settings.basePath + 'braincloud/termsize/' + id,
    context: document.body,
    success: function(data){
      $('#srvresponse').prepend('<p id="'+id+'">'+data+'</p>');
      $('#srvresponse').data("test" + id,data);
    },
    statusCode: {404: function() {
      alert('page not found');
    }},
    error: function() { 
        //alert("error"); 
    },
  });
  return $('#srvresponse').data("test" + id);
};