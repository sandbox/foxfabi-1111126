/**
Error: uncaught exception: [Exception... "An invalid or illegal string was specified"  code: "12" nsresult: "0x8053000c (NS_ERROR_DOM_SYNTAX_ERR)"  location: "http://192.168.56.101/projects/fabfolio/modules/views_braincloud/js/processing-1.1.0.js?D Line: 15440"]
**/

//**********************************************************************************//
class Ball {  
  float x, y, z;   // FIXME: ball center same as vect.center
  float initDepth; // term hierarchy depth
  float  w, h, volume, attempts, angle;  
  PVector center;
  float diameter;  
  float vx = 0;  
  float vy = 0;  
  int collisions = 0;   // collison counter
  String termname;
  int tid, index;  
  // Set drag switch to false
  boolean dragging=false;
  boolean hover = false;
  // Set maximum and minimum circle size
  int minSize = 4;
  int maxSize = minSize*minSize;
  color c;
  int highlight = 0;
  int thisTurn = 0;       // rememeber display status for main draw
  float size = 0;
  ArrayList childrens;		// list of childs
  ArrayList parents;			// list of parents
  ParticleSystem psys;
  int show = 20;
  float scaling;          // scaling

  float mass;  
  PVector vel, parentCoord;
  Ball(int indexin, int idin, String name, float size, float depth) {  
    this.index = indexin;  
    this.tid = idin;
    this.termname = name;
    this.size = size;
    this.initDepth = depth;
    this.minSize = minFontSize; //FIXME global..
    this.x = width/2;
    this.y = height/2;
    if (randomPos) {
      this.x += random(-width/2,width/2);  
      this.y += random(-height/2,height/2);  
    }
    //this.z = 0; //TODO: go 3D
    this.childrens = new ArrayList();
    this.parents = new ArrayList();
    this.mass = 1/(this.size+0.1);  
    this.vel = new PVector(random(-1,1), random(-1,1));
    this.parentCoord = new PVector(this.x, this.y);
    this.center = new PVector(this.x, this.y);        
    this.psystems = new ArrayList();  // effects list for highlight
    this.angle -= 0;

    // run this.lookup every 10 seconds
    // TODO: add radioactivity or something like that
    //       so term will animate a bit ;)
    if (ajaxReload == 1) {
      window.setInterval (lookup, random(14000,1500000) + 3000000*index/random(40,50));
    }
    this.scaleTerm();
    //this.h = int(96/100*this.h);
    textFont(font, this.h);
    this.w = textWidth(this.termname);
    this.volume = this.w*this.h;
    this.diameter = this.w + this.w/2;
    //addParent(0);
    this.c = color(random(120,255), random(120,255), 255, int(255*this.scaling));

    // query php script for new data
    function lookup() {
      int dynSize = int(get_tag_size(tid));
      //println("Size for hashtag:" + BallId + "=" + dynSize);
      if (dynSize) {
        if (dynSize != size) {
          size = dynSize;
          scaleTerm();
          //h = int(96/100*h);
          textFont(font, h);
          w = textWidth(termname);
          diameter = this.w + this.w/2;
          c = color(random(120,255), random(120,255), 255, int(255*scaling));
        }
      }
    }

  }   

  void addChild(id) {
    this.childrens.add(id);
  }

  void addParent(id) {
    this.parents.add(id);
  }


  void collide() { 
    for (int k = 0; k < numBalls; k++) {
        if (balls[k].tid != this.tid) {
          if (balls[k].initDepth == this.initDepth) {
            bounce(balls[k]);
          }
        }
    }
  }  

  void bounce(Ball from) { 
    float dx = from.x - this.x;  
    float dy = from.y - this.y;  
      
    // collision only if they have same depth / layer (on 3d have to change)
    PVector vecBetween  = PVector.sub(this.center, from.center);
    // get the length of the vector
    float distance = abs(int(vecBetween.mag()));
    float distance = sqrt(dx*dx + dy*dy);
    float minDist = 0.5*(from.w + this.w); 
    if (distance < minDist) {
      float angle = atan2(dy, dx);
      float targetX = this.x + cos(angle) * minDist;  
      float targetY = this.y + sin(angle) * minDist;  
      float ax = (targetX - from.x) * spring;  
      float ay = (targetY - from.y) * spring;  
      this.vel.x -= ax;  
      this.vel.y -= ay;  
      from.vel.x += ax;  
      from.vel.y += ay;  
      this.vel.limit(1);
      from.vel.limit(1);
    }
  } 

  void calcAngle() {
    for (int si = this.parents.size()-1; si >= 0; si--) {
      int spsys = int(this.parents.get(si));
      for (int sk = 0; sk < numBalls; sk++) {
        if (balls[sk].tid == spsys) {
            int childs = balls[sk].childrens.size();
            if (childs > 0) {
              this.angle = (this.index*PI/childs);
            }
        }
      }
    }
  }

  // checkBoundaryCollision() function:  
  // on boundary collision, change direction
  void checkBoundaryCollision() {  
    if (this.x > (width - this.w)){  
      this.x = width - this.w;  
      this.vel.x *= -1.0;
    }   
    else if (this.x < this.w){  
      this.x = this.w;  
      this.vel.x *= -1.0;
    }   

    if (this.y > (height - this.h)){  
      this.y = height - this.h;  
      this.vel.y *= -1.0;
    }   
    else if (this.y < this.h){  
      this.y = this.h;  
      this.vel.y *= -1.0;
    }  
  }  

  void move() {
    this.x += this.vel.x;
    this.y += this.vel.y;
    this.center = new PVector(this.x, this.y);    
  }

  void check() {
    //this.scaleTerm();               // scale term by size
    if (termAttraction) {
      this.update();                  // update action/reaction
    }
    this.checkBoundaryCollision();  // check bound collision
    this.move();                    // move obejczs
  }

  void run() {
    this.check();
    this.display();        
    // update all childs ... matching parents
    for (int i = this.childrens.size()-1; i >= 0; i--) {
      int psys = int(this.childrens.get(i));
      for (int k = 0; k < numBalls; k++) {
        if (balls[k].tid == psys) {
          if (balls[k].tid != this.tid) {
            balls[k].parentCoord = new PVector(this.x, this.y);
            balls[k].run();//(this.x, this.y);
          }
        }
      }
    }   					 
  }

  void update() {
    // if we are not showing main terms
    if (this.initDepth > 0) {
      // only childs will be attracted
      PVector vecBetween  = PVector.sub(this.center, this.parentCoord);
      // get the length of the vector
      float distBetween = vecBetween.mag();
      if (distBetween > (this.w + this.w/2)) {
        //println("HH" + this.termname + "|" + this.mass + "," + this.scaling + "," + this.size);
        PVector attraction = calculateAttraction(this.center, this.parentCoord, (width/0.01+this.scaling));
        PVector repulsion = calculateRepulsion(this.center, this.parentCoord, (this.w/0.01+this.mass));
        attraction.add(repulsion);
        this.vel.add(attraction);
        this.vel.limit(1);
      } else {
        this.vel = new PVector(random(-1,1), random(-1,1));
      }
    }
  }  

	/**
	 * Processing Useful Code Snippets: 
	 * calculate the force of attraction between two points of the type <PVector>
	 */
	PVector calculateAttraction(PVector child, PVector attractor, float scaling ) {
		// get the vector between the point and the attractor point
		PVector vecBetween  = PVector.sub(attractor, child);
		// get the length of the vector
		float   distBetween = vecBetween.mag();
		// normalize the vector
		vecBetween.normalize();
		// scale 1/distance to get a scalar effect -- closer means more force!
    if (distBetween != 0) {
		  vecBetween.mult( (1/distBetween) * scaling );
    }
		// store a copy of the result
		PVector attForce    = vecBetween.get();
		// return the result
    attForce.limit(1);
		return  attForce;		
	}

	/**
	 * Processing Useful Code Snippets: 
	 * calculate the force of repulsion between two points of the type <PVector>
	 */
	PVector calculateRepulsion(PVector child, PVector attractor, float scaling){
    // get the vector between the point and the attractor point
    PVector vecBetween  = PVector.sub(attractor, child);
    // get the length of the vector
    float distBetween = vecBetween.mag();
    // normalize the vector
    vecBetween.normalize();
    // scale 1/distance to get a scalar effect -- closer means more force!
    if (distBetween != 0) {
      vecBetween.mult( (1/distBetween) * scaling );
    }
    // multiply by minus one to repel
    vecBetween.mult(-1);
    // store a copy of the result
    PVector attForce    = vecBetween.get();
    // return the result
    attForce.limit(1);
    return  attForce;
	}

  void scaleTerm() {
    this.scaling = int(maxSize - (maxSize/minSize)*this.size);
    this.h = 1 + this.scaling;
    //println("jj" + this.size + ".." + this.scaling + "..." + this.h);
  }
	
  void display() { 
    if (this.thisTurn == 0) {
      //c = color(random(150,255), random(150,255), 255, 200);
      fill(c,200);
      if (this.initDepth > 0) {
        stroke(c,200);
      } else {
        fill(0,0,0,200);
        stroke(0,0,0,200);
      }
      //translate(-10*scaling,-10*scaling);
      this.scaleTerm();
      //h = int(96/100*h);
      textFont(font, this.h);
      this.w = textWidth(this.termname);
      this.diameter = this.w + this.w/2;
      text(this.termname, this.x - this.w/2, this.y + this.h/4);
      fill(64,128,187,20);
      stroke(64,128,187,20);
      // TODO:: allow user to choose bg: none, rect, circle
      // FIXME circle and rect is font.h dependent ..
      ellipse(this.x, this.y, this.diameter, this.diameter);
      //fill(64,128,187,200);
      //stroke(64,128,187,200);
      line(this.x, this.y, 10 + this.parentCoord.x, 10 + this.parentCoord.y);
      //rect(this.x, this.y, this.diamter, this.h);
      this.thisTurn = 1;
    } 
  }  
   
}
//**********************************************************************************//
