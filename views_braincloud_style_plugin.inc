<?php
// $Id: views_braincloud_style_plugin.inc 41 2011-03-30 15:28:14Z foxfabi $

/**
 * @file
 */

class views_braincloud_style_plugin extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();	
    //$options['braincloud_energy'] = array('default' => '1');
    $options['braincloud_vid'] = array('default' => '1');
    $options['braincloud_attract'] = array('default' => '1');
    $options['braincloud_collide'] = array('default' => '1');
    $options['braincloud_ajax'] = array('default' => '0');
    $options['braincloud_startpos'] = array('default' => '0');
    $options['braincloud_framerate'] = array('default' => '8');
    $options['braincloud_spring'] = array('default' => '0.05');
    $options['braincloud_bg'] = array('default' => '#FFFFFF');
    $options['braincloud_width'] = array('default' => '640');
    $options['braincloud_height'] = array('default' => '480');
    $options['braincloud_font'] = array('default' => '');
    $options['braincloud_fontsize'] = array('default' => '12');
    return $options;
  }

  /**
   * Provide form
   */
  function options_form(&$form, &$form_state) {

    // fonts are browser dependend, we should wait until
    // full support is given native
    $fonts = array(
      'Narkisim-48' => 'Narkisim-48',
    );

    $ajaxen = array(
                    '0' => t('Disabled'),
                    //'1' => t('Enabled'),
                  );
    $startpos = array(
                    '0' => t('Center'),
                    '1' => t('Random'),
                  );
    $actreact = array(
                    '0' => t('Disabled'),
                    '1' => t('Enabled'),
                  );

    // Get proper values for 'allowed_values_function', which is a core setting.
    $vocabularies = taxonomy_get_vocabularies();
    //$options = array();
    foreach ($vocabularies as $vocabulary) {
      $vocabs[$vocabulary->vid] = $vocabulary->name;
    }
	
    $form['braincloud_vid'] = array(
      '#title' => t('Vocabulary'),
      '#description' => t('Pick the vocabulary to show'),
      '#type' => 'select',
      '#options' => $vocabs,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_vid'],
    );

    $form['braincloud_display'] = array(
      '#type' => 'fieldset',
      '#title' => t('Term display'),
      '#collapsible' => TRUE,
      '#attributes' => array('class' => $module),
    );
    
    $form['braincloud_startpos'] = array(
      '#title' => t('Start position'),
      '#description' => t('Pick the start position of terms'),
      '#type' => 'select',
      '#options' => $startpos,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_startpos'],
    );
    
    $form['braincloud_font'] = array(
      '#title' => t('Font'),
      '#description' => t('Pick the font for displaying terms'),
      '#type' => 'select',
      '#options' => $fonts,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_font'],	
    );
	
    $fontsize_options = range(2, 20, 1);
    $fontsize_options = array_combine($fontsize_options, $fontsize_options);
    $form['braincloud_fontsize'] = array(
      '#title' => t('Font size'),
      '#description' => t('Pick the font size for displaying terms'),
      '#type' => 'select',
      '#options' => $fontsize_options,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_fontsize'],	
    );
	
    $form['braincloud_animation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Term animation'),
      '#collapsible' => TRUE,
      '#attributes' => array('class' => $module),
    );

    $form['braincloud_attract'] = array(
      '#title' => t('Attract'),
      '#description' => t('Enable parent attraction of term childs'),
      '#type' => 'select',
      '#options' => $actreact,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_attract'],
    );
    
    $form['braincloud_collide'] = array(
      '#title' => t('Bounce'),
      '#description' => t('Enable bounce of term childs'),
      '#type' => 'select',
      '#options' => $actreact,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_collide'],
    );

    
    $form['braincloud_ajax'] = array(
      '#title' => t('Update'),
      '#description' => t('Enable reload of term size on runtime'),
      '#type' => 'select',
      '#options' => $ajaxen,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_ajax'],
    );



    $framerate_options = range(1, 30, 1);
    $framerate_options = array_combine($framerate_options, $framerate_options);
    $form['braincloud_framerate'] = array(
      '#title' => t('Framerate'),
      '#description' => t('Specify the framerate to use for the animation'),
      '#type' => 'select',
      '#required' => TRUE, 
      '#options' => $framerate_options,      
      '#default_value' => $this->options['braincloud_framerate'],
    );

    $spring_options = range(0.05, 0.5, 0.05);
    $spring_options = array_combine($spring_options, $spring_options);
    $form['braincloud_spring'] = array(
      '#title' => t('Spring'),
      '#description' => t('Specify the spring to use for term reaction'),
      '#type' => 'select',
      '#options' => $spring_options,      
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_spring'],
    );

    $form['braincloud_canvas'] = array(
      '#type' => 'fieldset',
      '#title' => t('Canvas options'),
      '#collapsible' => TRUE,
      '#attributes' => array('class' => $module),
    );
    
    $form['braincloud_width'] = array(
      '#title' => t('Width'),
      '#description' => t('Specify the width of the animation canvas such 640 or <i>screen.width</i>'),
      '#type' => 'textfield',
      '#size' => 5,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_width'],
      '#extra_validate' => array('views_braincloud_size_element_validate'),
    );
    $form['braincloud_height'] = array(
      '#title' => t('Height'),
      '#description' => t('Specify the height of the animation canvas such 480 or <i>screen.height</i>'),
      '#type' => 'textfield',
      '#size' => 5,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_height'],
      '#element_validate' => array('views_braincloud_size_element_validate'),
    );
    $form['braincloud_bg'] = array(
      '#title' => t('Background color'),
      '#description' => t('Specify the color using hexadecimal notation such as #FFCC00'),
      '#type' => 'textfield',
      '#size' => 5,
      '#required' => TRUE, 
      '#default_value' => $this->options['braincloud_bg'],
      '#element_validate' => array('views_braincloud_hexcolor_element_validate'),
    );

  }

  
  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    //FIXME: element_validate is not fired, so do it manually

    // check color element
    $element = $form['braincloud_bg'];
    if (!$element['#required'] && empty($element['#value'])) {
      return;
    }
    // based on contributions/calendar/calendar.module, line 427
    if (!preg_match('/^#(?:(?:[a-f\d]{6}))$/i', $element['#value'])) {
      form_error($element, t('@value is not a valid hex color', array('@value' => check_plain($element['#value']))));
    }

    // check width element
    $element = $form['braincloud_width'];
    if (strstr($element['#value'], 'screen.')) {
    } else {
      if(!is_numeric($element['#value'])) {
        form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>numberic</em> value.'))));
      }
      /* must be higher than 0 */
      if ($element['#value'] <= 0) {
        form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>number</em> higher than 0.'))));
      }
    }

    // check height element
    $element = $form['braincloud_height'];
    if (strstr($element['#value'], 'screen.')) {
    } else {
      if(!is_numeric($element['#value'])) {
        form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>numberic</em> value.'))));
      }
      /* must be higher than 0 */
      if ($element['#value'] <= 0) {
        form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>number</em> higher than 0.'))));
      }
    }
    
  }
  
  function render() {
    $output = parent::render();
    if (isset($this->view->live_preview) && $this->view->live_preview) {
      return t('Braincloud View not compatible with live preview.');
    }				
    
    // FIXME:: get values from fieldset...
    $view_settings['braincloud_vid'] = $this->options['braincloud_vid'];
    $view_settings['braincloud_attract'] = $this->options['braincloud_attract'];
    $view_settings['braincloud_collide'] = $this->options['braincloud_collide'];
    $view_settings['braincloud_ajax'] = $this->options['braincloud_ajax'];
    $view_settings['braincloud_startpos'] = $this->options['braincloud_startpos'];
    $view_settings['braincloud_framerate'] = $this->options['braincloud_framerate'];
    $view_settings['braincloud_spring'] = $this->options['braincloud_spring'];
    $view_settings['braincloud_width'] = $this->options['braincloud_width'];
    $view_settings['braincloud_height'] = $this->options['braincloud_height'];
    $view_settings['braincloud_bg'] = $this->options['braincloud_bg'];
    $view_settings['braincloud_font'] = $this->options['braincloud_font'];
    $view_settings['braincloud_fontsize'] = $this->options['braincloud_fontsize'];
    return theme($this->theme_functions(), $this->view, $view_settings);
  }
}
